/*
    This project is created to practice probability calculations.
    The core is a Stack class that has n cards.
        It can:
            shuffle();      //shuffles the order of cards inside but doesn't change their values
            pick();         //pics a randomly selected card from the stack
    Cards are represented as integer numbers in N-number system where n is the number of unique symbols.
    This way you can easily tell what symbol your card is by "looking" at the first digit. 
    The second digit holds the "color" of the card.
    It is expected that there are only as many colours as there are unique symbols.
*/
function rng(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}


class Stack {
    //standard stack
    static standard = {
        colours: ["Clubs", "Diamonds", "Hearts", "Spades"],
        symbols: ["2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King", "Ace"]
    }

    //standard stack + 4 Jokers
    static standard_extended = {
        colours: ["Clubs", "Diamonds", "Hearts", "Spades"],
        symbols: ["2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King", "Ace", "Joker"]
    }

    //stack for prsi
    static prsi = {
        colours: ["Zelene", "Kule", "Zaludy", "Cervene"],
        symbols: ["VII", "VIII", "IX", "X", "Spodek", "Filek", "Kral", "Eso"]
    }

    constructor(params) {
        let parm;
        if (typeof (params) == 'string' || params instanceof String) parm = Stack[params];
        else if (params instanceof Object) {
            parm = params;
            if (parm.colours.length > parm.symbols.length) return undefined;
        }
        else parm = Stack.standard;
        this.colours = [... new Set(parm.colours)];
        this.upperColours = this.colours.map(str => { return str.toUpperCase() });

        this.symbols = [... new Set(parm.symbols)];
        this.upperSymbols = this.symbols.map(str => { return str.toUpperCase() });

        if (parm.cards) {
            this.cards = [... new Set(parm.cards)];
            this.original = this.cards;
        }
        else this.gen();
    }

    rng(diff = 0) {
        return Math.floor(Math.random() * (this.cards.length - diff));
    }

    gen() {
        let nSys = this.symbols.length;
        this.original = new Array(this.colours.length * nSys);

        for (let n = 0; n < this.original.length; n++) this.original[n] = n;
        this.cards = [...this.original];
        this.shuffle();
    }

    shuffle() {
        let rand = undefined;
        let temp = [...this.cards];
        for (let n = 0; n < this.cards.length; n++) {
            rand = this.rng(n);
            this.cards[n] = temp[rand];
            temp.splice(rand, 1);
        }
        this.print();
    }

    push(value, position = undefined) {
        try {
            if (value && !(this.cards.includes(value))) {
                if (value < this.symbols.length * this.colours.length) {
                    if (position && position < this.cards.length) {
                        this.cards.splice(position, 0, value);
                    }
                    else this.cards.push(value);
                }
                else throw new Error(`Value (${value}) is too big to be in the stack.`)
            }
            else throw new Error(`Value (${value}) is not defined or it is already present in the stack.`);
        }
        catch (err) {
            console.error(err);
        }
    }

    pop(val = undefined) {
        try {
            let pos;
            let stack = this;
            if (typeof (val) == 'number') pos = [val];
            else if (val instanceof Array) pos = val;
            else if (typeof (val) === 'string' || val instanceof String) pos = this.find(val);
            else return this.cards.shift();

            pos.sort((a, b) => { return (b - a) });
            return pos.map((val) => {
                if (val >= 0 && val < stack.cards.length)return this.cards.splice(val, 1)[0];
                return undefined;
            });
        }
        catch (err) {
            console.error(err);
        }
    }

    readable(num) {
        let res = {
            num: num,
            symb_n: num % this.symbols.length,
            col_n: Math.floor(num / (this.symbols.length)),
        }
        res.symbol = this.symbols[res.symb_n];
        res.color = this.colours[res.col_n];
        res.str = `${res.symbol} of ${res.color}`;
        return res;
    }

    find(val) {
        try {
            if (typeof (val) === 'string' || val instanceof String) {
                let input = val.trim().toUpperCase().split(' OF ');
                if (input.length == 1) {
                    let num;
                    //Check if colour of specified value exists
                    if ((num = this.upperColours.indexOf(input[0])) != -1) {
                        let ret = [];
                        this.cards.forEach((val, idx) => { if (this.readable(val).col_n == num) ret.push(idx) });
                        return ret;
                    }

                    //If such colour does not exist, check if such symbol exists
                    else if ((num = this.upperSymbols.indexOf(input[0])) != -1) {
                        let ret = [];
                        this.cards.forEach((val, idx) => { if (this.readable(val).symb_n == num) ret.push(idx) });
                        return ret;
                    }
                    //If both previous attempts failed the input is not valid.
                    else throw new Error(`Given string "${val}" is neither a symbol nor a colour.`);
                }
                //' OF ' was found in the val string
                else if (input.length == 2) {
                    let symb;
                    let col;

                    if ((symb = this.upperSymbols.indexOf(input[0]) != -1) && (col = this.upperColours.indexOf(input[1]) != -1)) {
                        let ret = [];
                        this.cards.forEach((val, idx) => {
                            let readable = this.readable(val);
                            if ((readable.symb_n == symb) && (readable.col_n == col)) ret.push(idx);
                        });
                        return ret;
                    }
                    else throw new Error(`Given string "${val}" was interpreted as symbol "${input[0]}" and colour "${input[1]}". At least one of which doesn't exist in it's respective array.`);
                }
                else throw new Error(`Given string "${val}" can't be used to find cards in a Stack.`);
            }
            else if (typeof (val) === 'number' || val instanceof Number) {
                if (val < this.colours.length * this.symbols.length) {
                    let res;
                    if ((res = this.cards.indexOf(val)) != -1) return [res];
                    else return [];
                }
                else throw new Error(`The specified number (${val}) is too large. The max value can be ${this.colours.length * this.symbols.length - 1}!`);
            }
            else if (val instanceof Object) {
                throw new Error(`This feature hasn't been implemented yet. :(`);
            }
            else throw new Error(`This function can't process your input. It only works with strings, numbers or objects.`);
        }
        catch (err) {
            console.error(err); return
        }
    }

    printSome(input) {
        let buff = `XXXXXXXXXXXXXXXXXXXX\n`
        if (input instanceof Array) {
            input.forEach(val => { buff += `${this.readable(this.cards[val]).str}\n` });
        }
        else buff += `${this.readable(this.cards[input]).str}\n`;

        buff += `XXXXXXXXXXXXXXXXXXXX`;
        console.log(buff);
    }

    print(cols = 1, delimiter = "") {
        let listAll = new Array(this.cards.length);
        let maxLen = 0;
        for (let n = 0; n < this.cards.length; n++) {
            listAll[n] = this.readable(this.cards[n]).str;
            if (listAll[n].length > maxLen) maxLen = listAll[n].length;
        }

        maxLen += 3;
        let buff = `>>>>>\nPrinting a stack of ${this.cards.length} cards.
        \rPossible symbols are: ${this.symbols}
        \rPossible colours: ${this.colours}\n`;

        listAll.forEach((val, idx, arr) => {
            if (!(idx % cols)) {
                buff += '\n';
                buff += ' > ';
            }
            buff += val.padEnd(maxLen, ' ') + delimiter;
        });
        console.log(buff + "\n<<<<<");
    }

    popProbabilityOf(cards) {
        let possib = this.find(cards);
        console.log(`Pop-probability of ${cards} is ${possib.length ? (100 * possib.length / this.cards.length).toPrecision(4) : 0}%.`);
    }

    reset() {
        this.cards = [...this.original];
    }
}
