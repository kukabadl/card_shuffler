# Card shuffler
## Purpose
This project was created just because I haven't done much programming lately nad also because I wanted to practise some probability calculations. It's main features are to:
1. Act as fully capable card stack capable of operation with custom types of stack.
    + Users can define their own "colours" and "symbols" when calling the constructor. This is done by passing an object containing at least 2 parameters:
        1. *"symbols" array of strings that are the names of individual symbols.*
        2. *"colours" array of strings that are the names of individual the colours. All symbols are created for each colour by default.*
        3. (optional) "cards" array of integers. Their interpretation is going to be explained later.
    + All cards are represented by integer values. The symbols are represented by the first "digit" the term digit is used very loosely. Stack operates in number system with base N where N is the number of all possible symbols. *note that you can only use "Stacks" that have more symbols than colours*
2. Act as a probability calculator of different events at any point during drawing cards
    
## How does it work
- The core is a Stack class that at max stores N cards where `N = colours.length * symbols.length`.
- Cards are represented as integer numbers in N-number system where n is the number of unique symbols.
    - This way you can easily tell what symbol your card is by "looking" at the first digit. 
    - The second digit holds the "color" of the card.
    - It is expected that there are only as many colours as there are unique symbols.
## API
- Create a new stack
    1. Creating standard Stack (using the default constructor):
        - `let defaultStack = new Stack();`
        - This way you will create standard stack consisting of 52 playing cards of 4 colours (Clubs, Diamonds, Hearts, Spades) and 13 symbols (2, 3, 4, 5, 6, 7, 8, 9, 10, Jack, Queen, King, Ace)
    2. Creating Stack from a predefined template (using predefined values of the Stack class):
        - Pass a string to the constructor which represents the name of the static variable holding the configuration for the Stack class.
        - So far you can choose from 3 predefined Stacks ("standard", "standard_extended", "prsi")
        - `let predefStack = new Stack("standard_extended");` 
    3. Creating a custom stack by specifying all colours and synbols:
    ```
        let customConfig = {
            colours: ["Clubs", "Diamonds", "Hearts", "Spades"],
            symbols: ["2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King", "Ace"],
            cards: [0, 22, 13, 5, 10, 15]
        };
        let newStack = new Stack(customConfig);
    ```
- Print the whole stack along with its "configuration".
    - `print(cols = 1, delimiter = "");`
        - Prints the whole stack and tells you how many cards it contains, which symbols may be found there and which colours the cards might have.
        - You can specify the number of columns the cards are going to be separated into by setting the `cols` parameter or you can set the `delimiter` between the columns.
- Print only cards of the specified positions.
    - `printSome(input);`
    - print one or more cards. `input` parameter can be either number or an array of numbers which specify the position of the card.
- Take one or more cards from the top of the Stack or elsewhere and return it's (their)indices.
    - `pop(val = undefined);`
        - If no parameters are passed it pops the card located "on top" of the stack.
    - `pop(2);`
        - If number or an array of numbers (`pop([1, 3, 6]);`) is passed then each element gets removed and returned in a new array.
- Put a card at the bottom of the stack or to the specified position
    - `push(value, position = undefined);`
    - If `position` is undefidned or if it is equal or larger than the total number of cards in the stack the card is pushed at the back.
    - If `position` is defined and smaller than the total number of cards than the card is inserted at the specified position.
- Find one or more cards in a stack by its value or symbol or colour and return it's (their) positions
    - `find(val);` returns an array of objects representing cards satisfying given criteria.
    - If `val` is a number only one card can be returned - only the one matching exactly the symbol and colour.
    - If you wish to search only by colour or symbol you need to specify it as a string e.g. `find("spades");` The string is processed case insensitively and can be used to specify colour, symbol or both.
    - If you want to specify both colour and symbol it can be done by separating them by a keyword `of`. Before the `of` keyword the symbol is specified and after it the colour is specified. For example to find Jakc of colour Hearts you would use it like this:
        - `list("Jack of spades");`
        - This returns the position of the card in the stack in an array (for the case when more cards are to be returned).
- Shuffle order of the cards in the stack
    - `shuffle();`
    - Shuffles the order of the cards in the stack but it doesn't change what cards are in it.
- Reset stack to the original state
    - `reset();`
    - Resets the stack to the state in which it was right after creation
- Calculate the probability of occurence of given cards;
    - `popProbabilityOf(cards);`
    -  `cards` parameter is a string that specifies the cards whose probability of occurence we want to calculate (syntax of the string is the same as with the `find();` function)